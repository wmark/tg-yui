# Release information about TGYUI

version = "2.5.0"

description = "The Yahoo! YUI packaged as collection of TurboGears widgets."
# long_description = "More description about your plan"
author = "W-Mark Kubacki, Marco Mariani, Lulamile Mzamo"
email = "wmark.tgyui@hurrikane.de"
copyright = "Authors; for static/yui: Copyright (c) 2006-2007, Yahoo! Inc."

# if it's open source, you might want to specify these
url = "http://tgwidgets.ossdl.de/"
download_url = "http://static.ossdl.de/tgwidgets/downloads/"
license = "BSD"
